import json
import os
import time
from datetime import datetime, timedelta
import tkinter as tk
from tkinter import messagebox, simpledialog
from tkinter import ttk
import random

# Constants
DATA_FILE_PATH = "piercing_data.json"
PIERCING_OPTIONS = {
	1: "Lobe",
	2: "Ear Cartilage",
	3: "Nostril",
	4: "Other"
}
ALLERGY_OPTIONS = [
	"Nickel", "Surgical Steel", "Gold 8k", "Gold 9k", "Gold 14k",
	"Gold 18k", "White Gold", "Rose Gold", "Copper (in silver)",
	"Acrylics", "Organics (bone, wood, etc.)", "Non-implant grade titanium",
	"Nickel free steel", "Silver, not 925/sterling"
]
EMOTICONS = [
	"(•‿•)", "(¬‿¬)", "(*^‿^*)", "(¬‿¬)", "(＾ｖ＾)", "(´ ∀ ` *)",
	"(o^▽^o)", "(´･ᴗ･`)", "(o´▽`o)", "(^人^)", "(o´∀`o)", "(´ ω `)",
	"(＠＾◡＾)", "(＾▽＾)", "(⌒‿⌒)", "＼(＾▽＾)／", "(o´▽`o)"
]

# Default Data Structure
DEFAULT_DATA = {
	"piercings": {},
	"allergies": []
}


# Helper Functions
def load_data():
	if not os.path.exists(DATA_FILE_PATH):
		with open(DATA_FILE_PATH, 'w') as file:
			json.dump(DEFAULT_DATA, file)
		return DEFAULT_DATA
	else:
		with open(DATA_FILE_PATH, 'r') as file:
			return json.load(file)


def save_data(data):
	with open(DATA_FILE_PATH, 'w') as file:
		json.dump(data, file)


def days_between(d1, d2):
	return abs((d2 - d1).days)


def calculate_healing(piercing_date, healing_weeks):
	today = datetime.now().date()
	piercing_date = datetime.strptime(piercing_date, "%Y-%m-%d").date()
	days_passed = days_between(today, piercing_date)
	weeks_passed = days_passed // 7
	remaining_weeks = healing_weeks - weeks_passed
	remaining_days = days_passed % 7
	return weeks_passed, remaining_weeks, remaining_days, days_passed


# GUI Application
class PiercingTrackerApp(tk.Tk):
	def __init__(self):
		super().__init__()
		self.title("Piercing Tracker")
		self.geometry("600x400")
		self.configure(bg='#2B2B2B')
		self.data = load_data()
		self.emoticon = random.choice(EMOTICONS)
		self.clean_data()  # Clean up existing data
		self.create_widgets()

	def clean_data(self):
		""" Ensure all piercing entries are lists and have required keys. """
		for key, value in self.data['piercings'].items():
			if not isinstance(value, list):
				self.data['piercings'][key] = [value]
			for entry in self.data['piercings'][key]:
				if 'status' not in entry:
					entry['status'] = 'healing'
		save_data(self.data)

	def create_widgets(self):
		self.label = tk.Label(self, text="Welcome to Piercing Tracker", bg='#2B2B2B', fg='#FFFFFF')
		self.label.pack(pady=10)

		self.piercings_button = tk.Button(self, text="Your Piercings", command=self.manage_piercings, bg='#3C3F41',
		                                  fg='#FFFFFF')
		self.piercings_button.pack(pady=5)

		self.allergies_button = tk.Button(self, text="Allergies", command=self.manage_allergies, bg='#3C3F41',
		                                  fg='#FFFFFF')
		self.allergies_button.pack(pady=5)

		self.emoticon_label = tk.Label(self, text=self.emoticon, bg='#2B2B2B', fg='#FFFFFF', font=("Helvetica", 20))
		self.emoticon_label.pack(side=tk.BOTTOM, pady=10)

		self.update_display()

	def update_display(self):
		if not self.data['piercings']:
			self.label.config(text="No piercings! Select which piercings you have.")
			self.manage_piercings()
		else:
			self.label.config(text="Your Piercings")
			self.show_piercings_status()

	def manage_piercings(self):
		self.label.config(text="Select which piercings you have:")
		self.piercings_button.pack_forget()
		self.allergies_button.pack_forget()

		for key, value in PIERCING_OPTIONS.items():
			tk.Button(self, text=value, command=lambda k=key: self.add_piercing(k), bg='#3C3F41', fg='#FFFFFF').pack(
				pady=5)

		tk.Button(self, text="Done", command=self.update_display, bg='#3C3F41', fg='#FFFFFF').pack(pady=5)

	def add_piercing(self, piercing_type):
		if piercing_type == 4:
			piercing_name = simpledialog.askstring("Custom Piercing", "Enter the name of the piercing:")
			date_str = simpledialog.askstring("Piercing Date", "Enter the date you got this piercing (YYYY-MM-DD):")
		else:
			piercing_name = PIERCING_OPTIONS[piercing_type]
			date_str = simpledialog.askstring("Piercing Date",
			                                  f"Enter the date you got your {piercing_name} piercing (YYYY-MM-DD):")

		if piercing_name and date_str:
			piercing_entry = {
				"date": date_str,
				"healing_weeks": 6 if piercing_name == "Lobe" else 12,
				"status": "healing"
			}
			if piercing_name not in self.data['piercings']:
				self.data['piercings'][piercing_name] = []
			self.data['piercings'][piercing_name].append(piercing_entry)
			save_data(self.data)
			self.update_display()

	def manage_allergies(self):
		self.label.config(text="Manage your allergies:")
		self.piercings_button.pack_forget()
		self.allergies_button.pack_forget()

		for i, allergy in enumerate(ALLERGY_OPTIONS, 1):
			tk.Button(self, text=allergy, command=lambda a=allergy: self.add_allergy(a), bg='#3C3F41',
			          fg='#FFFFFF').pack(pady=2)

		tk.Button(self, text="Delete all entries", command=self.delete_all_allergies, bg='#3C3F41', fg='#FFFFFF').pack(
			pady=5)
		tk.Button(self, text="Done", command=self.update_display, bg='#3C3F41', fg='#FFFFFF').pack(pady=5)

	def add_allergy(self, allergy):
		if allergy not in self.data['allergies']:
			self.data['allergies'].append(allergy)
			save_data(self.data)

	def delete_all_allergies(self):
		self.data['allergies'] = []
		save_data(self.data)
		self.update_display()

	def show_piercings_status(self):
		for widget in self.winfo_children():
			if widget != self.emoticon_label:
				widget.pack_forget()

		self.label.pack(pady=10)
		self.piercings_button.pack(pady=5)
		self.allergies_button.pack(pady=5)

		for piercing, instances in self.data['piercings'].items():
			for i, info in enumerate(instances):
				print(f"Processing {piercing} instance {i}: {info}")  # Debug print
				if isinstance(info, dict):  # Ensure info is a dictionary
					weeks_passed, remaining_weeks, remaining_days, days_passed = calculate_healing(info['date'], info[
						'healing_weeks'])
					status_text = f"Time passed since getting your {piercing} piercing(s) ({i + 1}): {days_passed} days ({weeks_passed} weeks, {remaining_days} days).\n"
					if info['status'] == "healing":
						if weeks_passed >= 6:
							status_text += "Do you think they have healed and have managed to change them with success? "
							yes_button = tk.Button(self, text="Yes",
							                       command=lambda p=piercing, idx=i: self.update_healing_status(p, idx,
							                                                                                    True),
							                       bg='#3C3F41', fg='#FFFFFF')
							yes_button.pack()
							no_button = tk.Button(self, text="No",
							                      command=lambda p=piercing, idx=i: self.update_healing_status(p, idx,
							                                                                                   False),
							                      bg='#3C3F41', fg='#FFFFFF')
							no_button.pack()
						elif weeks_passed >= 8:
							info['status'] = "healed"
							save_data(self.data)
							status_text += "They should be healed."
						else:
							status_text += f"Remaining to heal: {remaining_weeks} weeks or {remaining_weeks * 7 + remaining_days} days."
					elif info['status'] == "healed":
						status_text += "Your piercing is fully healed!"
						if weeks_passed >= 6:
							status_text += "\nYou can now wear sterling silver, hoops, drops, etc."
						elif weeks_passed >= 3:
							status_text += "\nYou can now wear small huggies."

					tk.Label(self, text=status_text, bg='#2B2B2B', fg='#FFFFFF').pack(pady=5)

					progress = (weeks_passed / info['healing_weeks']) * 100
					progress_bar = ttk.Progressbar(self, length=200, mode='determinate')
					progress_bar['value'] = progress
					progress_bar.pack(pady=5)
				else:
					print(f"Unexpected data format: {info}")  # Debug print

	def update_healing_status(self, piercing, index, healed):
		if healed:
			self.data['piercings'][piercing][index]['status'] = "healed"
		else:
			self.data['piercings'][piercing][index]['healing_weeks'] += 1
		save_data(self.data)
		self.update_display()


if __name__ == "__main__":
	app = PiercingTrackerApp()
	app.mainloop()
